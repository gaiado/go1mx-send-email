<!DOCTYPE html>
<html lang='es'>

<head>
    <meta charset='UTF-8'>
    <title>Título del correo electrónico</title>
</head>

<body>
    <center>
        <table width='600px' cellspacing='0' cellpadding='0'>
            <tr>
                <td style='background-color:#067c96;padding-top:12px;padding-bottom:12px;padding-right:24px;padding-left:24px'
                    class='mceBlockContainer' valign='top'>
                    <div data-block-id='15' class='mceText' id='dataBlockId-15' style='width:100%'>
                        <h1 style='text-align: center;' class='last-child'><span style='color:#ffffff;'>Event reminder for <?php echo $event->post_title ?></span></h1>
                    </div>
                </td>
            </tr>
            <tr>
                <td align='center'>
                    <img width='300px' src='https://www.gracehealinggroup.com/wp-content/uploads/2023/04/1.png'
                        alt='gracehealinggroup'>
                </td>
            </tr>
            <tr>
                <td>
                    <p><?php echo $event->post_excerpt ?></p>
                </td>
            </tr>
            <tr>
                <td align='center'>
                    <a target='_blank' href='<?php echo $event->guid ?>'
                        style='display:inline-block; background-color:#067c96; color:#fff; padding: 10px 20px; text-decoration:none;'>CLICK HERE</a>
                </td>
            </tr>
            <tr>
                <td style='background-color:transparent;padding-top:20px;padding-bottom:20px;'
                    class='mceBlockContainer' valign='top'>
                    <table border='0' cellpadding='0' cellspacing='0' width='100%' style='background-color:transparent'
                        role='presentation' data-block-id='6'>
                        <tbody>
                            <tr>
                                <td style='min-width:100%;border-top:2px solid #000000' valign='top'></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td style='padding-top:12px;padding-bottom:12px;padding-right:0;padding-left:0'
                    class='mceLayoutContainer' valign='top'>
                    <table align='center' border='0' cellpadding='0' cellspacing='0' width='100%' role='presentation'
                        data-block-id='7'>
                        <tbody>
                            <tr class='mceRow'>
                                <td style='background-position:center;background-repeat:no-repeat;background-size:cover'
                                    valign='top'>
                                    <table border='0' cellpadding='0' cellspacing='24' width='100%' role='presentation'>
                                        <tbody>
                                            <tr>
                                                <td style='margin-bottom:24px' class='mceColumn' data-block-id='-9'
                                                    valign='top' colspan='12' width='100%'>
                                                    <table border='0' cellpadding='0' cellspacing='0' width='100%'
                                                        role='presentation'>
                                                        <tbody>
                                                            <tr>
                                                                <td align='center' valign='top'>
                                                                    <table border='0' cellpadding='0' cellspacing='0'
                                                                        width='' role='presentation'
                                                                        class='mceClusterLayout' data-block-id='-8'>
                                                                        <tbody>
                                                                            <tr>
                                                                                <td style='padding-left:24px;padding-top:0;padding-right:24px'
                                                                                    data-breakpoint='6' valign='top'
                                                                                    class='mobileClass-6'><a
                                                                                        href='mailto:info@gracehealinggroup.com'
                                                                                        style='display:block'
                                                                                        target='_blank'
                                                                                        data-block-id='-5'><img
                                                                                            width='40'
                                                                                            style='border:0;width:40px;height:auto;max-width:100%;display:block'
                                                                                            alt='Email icon'
                                                                                            src='https://dim.mcusercontent.com/https/cdn-images.mailchimp.com%2Ficons%2Fsocial-block-v3%2Fblock-icons-v3%2Femail-filled-color-40.png?w=40&amp;dpr=2'
                                                                                            class=''></a></td>
                                                                                <td style='padding-left:24px;padding-top:0;padding-right:24px'
                                                                                    data-breakpoint='6' valign='top'
                                                                                    class='mobileClass-6'><a
                                                                                        href='https://instagram.com/gracehealinggroup'
                                                                                        style='display:block'
                                                                                        target='_blank'
                                                                                        data-block-id='-6'><img
                                                                                            width='40'
                                                                                            style='border:0;width:40px;height:auto;max-width:100%;display:block'
                                                                                            alt='Instagram icon'
                                                                                            src='https://dim.mcusercontent.com/https/cdn-images.mailchimp.com%2Ficons%2Fsocial-block-v3%2Fblock-icons-v3%2Finstagram-filled-color-40.png?w=40&amp;dpr=2'
                                                                                            class=''></a></td>
                                                                                <td style='padding-left:24px;padding-top:0;padding-right:24px'
                                                                                    data-breakpoint='6' valign='top'
                                                                                    class='mobileClass-6'><a
                                                                                        href='https://tiktok.com/@gracehealinggroup'
                                                                                        style='display:block'
                                                                                        target='_blank'
                                                                                        data-block-id='-7'><img
                                                                                            width='40'
                                                                                            style='border:0;width:40px;height:auto;max-width:100%;display:block'
                                                                                            alt='TikTok icon'
                                                                                            src='https://dim.mcusercontent.com/https/cdn-images.mailchimp.com%2Ficons%2Fsocial-block-v3%2Fblock-icons-v3%2Ftiktok-filled-color-40.png?w=40&amp;dpr=2'
                                                                                            class=''></a></td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td style='padding-top:8px;padding-bottom:8px;padding-right:8px;padding-left:8px'
                    class='mceLayoutContainer' valign='top'>
                    <table align='center' border='0' cellpadding='0' cellspacing='0' width='100%' role='presentation'
                        data-block-id='11' id='section_11e5addab6048d77811fe10fd6036c38' class='mceFooterSection'>
                        <tbody>
                            <tr class='mceRow'>
                                <td style='background-position:center;background-repeat:no-repeat;background-size:cover'
                                    valign='top'>
                                    <table border='0' cellpadding='0' cellspacing='12' width='100%' role='presentation'>
                                        <tbody>
                                            <tr>
                                                <td style='padding-top:0;padding-bottom:0;margin-bottom:12px'
                                                    class='mceColumn' data-block-id='-3' valign='top' colspan='12'
                                                    width='100%'>
                                                    <table border='0' cellpadding='0' cellspacing='0' width='100%'
                                                        role='presentation'>
                                                        <tbody>
                                                            <tr>
                                                                <td style='padding-top:12px;padding-bottom:12px;padding-right:48px;padding-left:48px'
                                                                    class='mceBlockContainer' align='center'
                                                                    valign='top'><img data-block-id='8' width='130'
                                                                        style='width:130px;height:auto;max-width:100%;display:block'
                                                                        alt='Logo'
                                                                        src='https://dim.mcusercontent.com/cs/5ffea896c3f33826841c79405/images/bdf6d631-25f4-2a13-d5fe-4c9305b14d5c.png?w=130&amp;dpr=2'
                                                                        class=''></td>
                                                            </tr>
                                                            <tr>
                                                                <td style='padding-top:12px;padding-bottom:12px;padding-right:24px;padding-left:24px'
                                                                    class='mceBlockContainer' align='center'
                                                                    valign='top'>
                                                                    <div data-block-id='9' class='mceText'
                                                                        id='dataBlockId-9'
                                                                        style='display:inline-block;width:100%'>
                                                                        <p class='last-child'><em><span
                                                                                    style='font-size: 12px'>Copyright
                                                                                    (C) 2023 Grace Healing Group. All
                                                                                    rights
                                                                                    reserved.</span></em><br><span
                                                                                style='font-size: 12px'>You are
                                                                                receiving this email because you opted
                                                                                in via our website.</span><br><br><span
                                                                                style='font-size: 12px'>Our mailing
                                                                                address is:</span><br><span
                                                                                style='font-size: 12px'></span></p>
                                                                        <div class='vcard'><span class='org fn'>Grace
                                                                                Healing Group</span>
                                                                            <div class='adr'>
                                                                                <div class='street-address'>159 W Green
                                                                                    St Unit 508</div><span
                                                                                    class='locality'>Pasadena</span>,
                                                                                <span class='region'>CA</span> <span
                                                                                    class='postal-code'>91105-4601</span>
                                                                            </div><br><a
                                                                                href='https://gracehealinggroup.us9.list-manage.com/vcard?u=5ffea896c3f33826841c79405&amp;id=81bea34334'
                                                                                class='hcard-download'>Add us to your
                                                                                address book</a>
                                                                        </div><br><br><span style='font-size: 12px'>Want
                                                                            to change how you receive these
                                                                            emails?</span><br><span
                                                                            style='font-size: 12px'>You can </span><a
                                                                            href='https://gracehealinggroup.us9.list-manage.com/profile?u=5ffea896c3f33826841c79405&amp;id=81bea34334&amp;e=[UNIQID]&amp;c=092aa1fcb1'><span
                                                                                style='font-size: 12px'>update your
                                                                                preferences</span></a><span
                                                                            style='font-size: 12px'> or </span><a
                                                                            href='https://gracehealinggroup.us9.list-manage.com/unsubscribe?u=5ffea896c3f33826841c79405&amp;id=81bea34334&amp;e=[UNIQID]&amp;c=092aa1fcb1'><span
                                                                                style='font-size: 12px'>unsubscribe</span></a>
                                                                        <p></p>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class='mceLayoutContainer' align='center'
                                                                    valign='top'>
                                                                    <table align='center' border='0' cellpadding='0'
                                                                        cellspacing='0' width='100%' role='presentation'
                                                                        data-block-id='-2'>
                                                                        <tbody>
                                                                            <tr class='mceRow'>
                                                                                <td style='background-position:center;background-repeat:no-repeat;background-size:cover;padding-top:0px;padding-bottom:0px'
                                                                                    valign='top'>
                                                                                    <table border='0' cellpadding='0'
                                                                                        cellspacing='24' width='100%'
                                                                                        role='presentation'>
                                                                                        <tbody></tbody>
                                                                                    </table>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </table>
    </center>
</body>

</html>