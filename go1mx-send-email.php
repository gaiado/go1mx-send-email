<?php

/*
Plugin Name: Go1 Send Email
Plugin URI: go1.mx
Description: Plugin to notify of Upcoming Events
Version: 0.1
Author: Go1
Author URI: go1.mx
*/

function go1mx_get_email($event){
  ob_start();
  include "mail.php";
  return ob_get_clean();
}

function go1mx_events(){
  return tribe_get_events( [
    'posts_per_page' => 22,
    //'start_date'     => 'now',
  ]);
}

function go1mx_headers(){

  $mailchimp_api_key = '60aaf2a995cf21251c10591296d9ec96-us9';
  $headers = array(
    'Authorization: Basic '. base64_encode('apikey:'.$mailchimp_api_key),
    'Content-Type: application/json',
  );
  return implode("\r\n", $headers);
}

function go1mx_campaigns($evento){
  $headers =  go1mx_headers();
  $segment_id = get_post_meta($evento->ID, 'segment_id', true);

  if(empty($segment_id)){
    return null;
  }

  $data = array(
    "type" => "regular",
    "recipients" => array(
        "list_id" => "81bea34334",
        "segment_opts" => array(
          "saved_segment_id"=> intval($segment_id),
        )
    ),
    "settings" => array(
        "subject_line" => "Event reminder",
        "title" => "Event reminder " . $evento->post_title,
        "from_name" => "Grace Healing Group",
        "reply_to" => "gracehealinggroup@gmail.com"
    ),
    "content_type" => "multichannel"
  );

  $options = array(
      'http' => array(
          'method' => 'POST',
          'header' => $headers,
          'content'=>  json_encode($data)
      ),
  );

  // Create a new stream context with the options
  $context = stream_context_create($options);
  // Send a GET request to retrieve all lists
  $response = file_get_contents('https://us9.api.mailchimp.com/3.0/campaigns', false, $context);

  return json_decode($response);
}

function go1mx_create_segments($post_id){
  
  $headers =  go1mx_headers();
  $data = array(
    "name" => "api_event_$post_id",
    "static_segment" => array_unique(get_post_meta($post_id, 'suscribed_users')),
  );
  $options = array(
      'http' => array(
          'method' => 'POST',
          'header' => $headers,
          'content'=>  json_encode($data)
      ),
  );

  // Create a new stream context with the options
  $context = stream_context_create($options);
  // Send a GET request to retrieve all lists
  $response = file_get_contents('https://us9.api.mailchimp.com/3.0/lists/81bea34334/segments', false, $context);

  $segment = json_decode($response);

  var_dump($segment);

  update_post_meta($post_id, 'segment_id', $segment->id);

  return $segment;
}

function go1mx_create_member($post_id,$email){
  
  $headers =  go1mx_headers();
  $data = array(
    "email_address" => $email,
    "status"=> "subscribed"
  );
  $options = array(
      'http' => array(
          'method' => 'POST',
          'header' => $headers,
          'content'=>  json_encode($data)
      ),
  );

  // Create a new stream context with the options
  $context = stream_context_create($options);
  // Send a GET request to retrieve all lists
  $response = file_get_contents('https://us9.api.mailchimp.com/3.0/lists/81bea34334/members', false, $context);

  add_post_meta($post_id, 'suscribed_users', $email);

  return json_decode($response);
}

function go1mx_update_segments($post_id, $segment_id){
  $headers =  go1mx_headers();
  $data = array(
    "name" => "api_event_$post_id",
    "static_segment" => array_unique(get_post_meta($post_id, 'suscribed_users')),
  );
  $options = array(
      'http' => array(
          'method' => 'PATCH',
          'header' => $headers,
          'content'=>  json_encode($data)
      ),
  );

  // Create a new stream context with the options
  $context = stream_context_create($options);
  // Send a GET request to retrieve all lists
  $response = file_get_contents('https://us9.api.mailchimp.com/3.0/lists/81bea34334/segments/'.$segment_id, false, $context);

  $segment = json_decode($response);

  return $segment;
}

function go1mx_campaigns_contents($campaign_id,$event){
  $headers =  go1mx_headers();
  $html = go1mx_get_email($event);
  $data = array(
    "html" => $html
  );

  $options = array(
    'http' => array(
        'method' => 'PUT',
        'header' => $headers,
        'content'=>  json_encode($data)
    ),
  );

  // Create a new stream context with the options
  $context = stream_context_create($options);
  // Send a GET request to retrieve all lists
  $response = file_get_contents("https://us9.api.mailchimp.com/3.0/campaigns/$campaign_id/content", false, $context);

  return json_decode($response);
}

function go1mx_campaigns_send($campaign_id){
  $headers =  go1mx_headers();
  $data = array(
  );

  $options = array(
    'http' => array(
        'method' => 'POST',
        'header' => $headers,
        'content'=>  json_encode($data)
    ),
  );

  // Create a new stream context with the options
  $context = stream_context_create($options);
  // Send a GET request to retrieve all lists
  $response = file_get_contents("https://us9.api.mailchimp.com/3.0/campaigns/$campaign_id/actions/send", false, $context);

  return json_decode($response);
}

function go1mx_campaigns_test($campaign_id){
  $headers =  go1mx_headers();
  $data = array(
    "test_emails" => [
      "pootalejandro@yahoo.fr",
      "carlos@go1.mx"
    ],
    "send_type"=>"html"
  );

  $options = array(
    'http' => array(
        'method' => 'POST',
        'header' => $headers,
        'content'=>  json_encode($data)
    ),
  );

  // Create a new stream context with the options
  $context = stream_context_create($options);
  // Send a GET request to retrieve all lists
  $response = file_get_contents("https://us9.api.mailchimp.com/3.0/campaigns/$campaign_id/actions/test", false, $context);

  return json_decode($response);
}

function go1mx_sendmails(){
  $eventos = go1mx_events();
  $campaigns = [];
  foreach ($eventos as $evento) {
    $campaign = go1mx_campaigns($evento);
    if($campaign){  
      go1mx_campaigns_contents($campaign->id, $evento);
      go1mx_campaigns_test($campaign->id);
      //go1mx_campaigns_send($campaign->id);
      $campaigns[]=$campaign;
    }
  }
  return $campaigns;
}

function go1mx_suscribe($data){
  $id = $data["id"];
  $user_id = $data["user_id"];
  $user = get_user_by( 'id', $user_id );
  go1mx_create_member($id, $user->user_email);
  $segment_id = get_post_meta($id, 'segment_id', true);
  var_dump($segment_id);
  if(empty($segment_id)){
     go1mx_create_segments($id);
  }else{
     go1mx_update_segments($id, $segment_id);
  }

  $location = $_SERVER['HTTP_REFERER'];
  wp_safe_redirect($location);
}

function go1mx_users(){
  $args = array(
    'role'    => 'subscriber'
  );
  return  get_users( $args );
}

add_action("rest_api_init", function () {
  register_rest_route("go1mx/v1", "/events", [
    "methods" => "GET",
    "callback" => "go1mx_sendmails",
  ]);
});


add_action("rest_api_init", function () {
  register_rest_route("go1mx/v1", "/suscribe/(?P<id>\d+)/(?P<user_id>\d+)", [
    "methods" => "GET",
    "callback" => "go1mx_suscribe",
  ]);
});


add_filter( 'wp_mail_from_name', function ( $original_email_from ) {
  return 'Grace Healing Group';
} );
